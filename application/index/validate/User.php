<?php
namespace app\index\validate;

use think\Validate;

class User extends Validate
{
    protected $rule = [
        'name|姓名' => 'require',
        'password|密码' => 'require',
        'captcha|验证码' => 'require|captcha',
    ];

    protected $msg = [
        'name.require' => '请输入姓名',
        'password.require' => '请输入密码',
        'captcha.require' => '请输入验证码',
    ];
    protected $scene = [
              'user'  =>  ['name','password','captcha'],
          ];
      
}
