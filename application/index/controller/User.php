<?php
namespace app\index\controller;

use app\index\model\User as UserModel;
use think\Db;
use think\Request;
use think\Session;

class User extends Base
{
    //登陆界面
    public function login()
    {
        $this->alreadyLogin();
        $this->view->assign('title', '用户登录');
        $this->view->assign('keywords', 'exam.com');
        $this->view->assign('desc', '在线考试系统');
        $this->view->assign('copyRight', '本网站仅供学习交流,请勿商用,责任自负');
        return $this->view->fetch();
    }

    //登录验证
    public function checkLogin()
    {
        $status = 0; //验证失败标志
        $result = '验证失败'; //失败提示信息
        $name = $_POST["name"];
        $password = $_POST["password"];
        $captcha = $_POST["captcha"];
        $role = $_POST["role"];
        $data = [
            'name' => $name,
            'password' => $password,
            'captcha' => $captcha,
        ];
        //验证数据
        if (!$name) {
            return ['status' => $status, 'message' => '用户名不能为空'];
        }

        if (!$password) {
            return ['status' => $status, 'message' => '密码不能为空'];
        }

        if (!$captcha) {
            return ['status' => $status, 'message' => '请输入验证码'];
        }

        if (!captcha_check($captcha)) {
            return ['status' => $status, 'message' => '验证码错误'];
        }

        //通过验证后,进行数据表查询
        //此处必须全等===才可以,因为验证不通过,$result保存错误信息字符串,返回非零
        if ($role == 2) {
            $user = Db::name('user')->where(['name' => $data['name']])->find();
        }
        if ($role == 3) {
            $user = Db::name('teacher')->where(['id' => $data['name']])->find();
        }
        if ($role == 4) {
            $user = Db::name('student')->where(['id' => $data['name']])->find();
        }
        if ($user) {
            if ($user['password'] == md5($data['password'])) {
                $status = 1;
                $result = '验证通过,点击[确定]后进入后台';

                //创建2个session,用来检测用户登陆状态和防止重复登陆
                Session::set('user_id', $user['id']);
                Session::set('user_role', $user['role']);
                Session::set('user_info', $user);
            } else {
                $result = '密码错误';
            }
        } else {
            $result = '没有该用户,请检查';
        }

        return ['status' => $status, 'message' => $result, 'data' => $user, 'role' => $user['role']];
    }

    //退出登陆
    public function logout()
    {
        
        Session::delete('user_id');
        Session::delete('user_role');
        Session::delete('user_info');

        $this->success('注销登陆,正在返回', url('user/login'));
    }

    //管理员列表
    public function adminList()
    {
        $this->view->assign('title', '管理员列表');
        $this->view->assign('keywords', '在线考试系统');
        $this->view->assign('desc', '在线考试');
        $this->view->count = UserModel::where('role', 1)->whereOr('role', 2)->count();

        //判断当前是不是admin用户
        //先通过session获取到用户登陆名
        $role = Session::get('user_role');
        $id = Session::get('user_id');
        $userName = Session::get('user_info.name');
        if ($role == '0' || $role =='1') {
            $list = UserModel::where(['role'=> 1])->whereOr(['role'=> 2])->paginate(10); //admin用户可以查看所有记录,数据要经过模型获取器处理
        } else {
            //为了共用列表模板,使用了all(),其实这里用get()符合逻辑,但有时也要变通
            //非admin只能看自己信息,数据要经过模型获取器处理
            $list = UserModel::all(['id' => $id]);
        }

        $this->view->assign('list', $list);
        $role = Session::get('user_role');
        $this -> view -> assign('role', $role);
        $this -> view -> assign('name', $userName);
        //渲染管理员列表模板
        return $this->view->fetch('admin_list');
    }

    //渲染编辑管理员界面
    public function adminEdit(Request $request)
    {
        $user_id = $request->param('id');
        $result = UserModel::get($user_id);
        $role = Session::get('user_role');
        $this->view->assign('title', '编辑管理员信息');
        $this->view->assign('keywords', 'exam.com');
        $this->view->assign('desc', '在线考试系统');
        $this->view->assign('user_info', $result->getData());
        $this->view->assign('role',$role);
        return $this->view->fetch('admin_edit');
    }

    //更新数据操作
    public function editUser(Request $request)
    {
        //获取表单返回的数据
        $param = $request->param();

        //去掉表单中为空的数据,即没有修改的内容
        foreach ($param as $key => $value) {
            if (!empty($value)) {
                $data[$key] = $value;
            }
        }

        $condition = ['id' => $data['id']];
        $result = UserModel::update($data, $condition);

        //如果是admin用户,更新当前session中用户信息user_info中的角色role,供页面调用
        if (Session::get('user_info.name') == 'admin') {
            Session::set('user_info.role', $data['role']);
        }

        if (true == $result) {
            return ['status' => 1, 'message' => '更新成功'];
        } else {
            return ['status' => 0, 'message' => '更新失败,请检查'];
        }
    }

    //删除操作
    public function deleteUser(Request $request)
    {
        $user_id = $request->param('id');
        UserModel::where('id', $user_id)->delete();

    }

    //添加操作的界面
    public function adminAdd()
    {
        $this->view->assign('title', '添加管理员');
        $this->view->assign('keywords', 'exam.com');
        $this->view->assign('desc', '在线考试系统');
        $role = Session::get('user_role');
        $this->view->assign('role',$role);
        return $this->view->fetch('admin_add');
    }

    //检测用户名是否可用
    public function checkUserName(Request $request)
    {
        $userName = trim($request->param('name'));
        $status = 1;
        $message = '用户名可用';
        if (UserModel::get(['name' => $userName])) {
            //如果在表中查询到该用户名
            $status = 0;
            $message = '用户名重复,请重新输入~~';
        }
        return ['status' => $status, 'message' => $message];
    }

    //检测用户邮箱是否可用
    public function checkUserEmail(Request $request)
    {
        $userEmail = trim($request->param('email'));
        $status = 1;
        $message = '邮箱可用';

        if (UserModel::get(['email' => $userEmail])) {
            //查询表中找到了该邮箱,修改返回值
            $status = 0;
            $message = '邮箱重复,请重新输入~~';
        }
        return ['status' => $status, 'message' => $message];
    }

    //检测用户手机号是否可用
    public function checkUserMobile(Request $request)
    {
        $userMobile = trim($request->param('mobile'));
        $status = 1;
        $message = '手机号可用';
        if (UserModel::get(['mobile' => $userMobile])) {
            //查询表中找到了该邮箱,修改返回值
            $status = 0;
            $message = '手机号重复,请重新输入~~';
        }
        return ['status' => $status, 'message' => $message];
    }

    //添加用户操作
    public function addUser()
    {
        /*添加用户*/
        $status = 1;
        $message = '添加成功';
        $data = [
            'name' => $_POST["name"],
            'password' => $_POST["password"],
            'email' => $_POST["email"],
            'mobile' => $_POST["mobile"],
            'role' => $_POST["role"],
            'sex' => $_POST["sex"],
        ];
        if ($data['name'] == null || $data['password'] == null || $data['email'] == null || $data['mobile'] == null) {
            //必填项不能为空
            $status = 0;
            $message = '请输入必填(*)信息';
        } else if (UserModel::where(['name' => $data['name']])->select() || UserModel::where(['email' => $data['email']])->select() || UserModel::where(['mobile' => $data['mobile']])->select()) {
            //如果在表中查询到该用户名
            $status = 0;
            $message = '请输入正确信息';
        } else {
            $user = UserModel::create($data);
            if ($user === null) {
                $status = 0;
                $message = '添加失败';
            }
        }

        return json(['status' => $status, 'message' => $message]);
    }

}
