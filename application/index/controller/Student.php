<?php
namespace app\index\controller;

use app\index\model\PaperUser as PaperUserModel;
use app\index\model\Student as StudentModel;
use think\Request;
use think\Session;

class Student extends Base
{
    //渲染学生个人信息列表
    public function studentDetail()
    {
        $userName = Session::get('user_id');

        //获取学生数据
        $studentList = StudentModel::where(['id' => $userName])->find();

        $this->view->assign('studentList', $studentList);
        $role = Session::get('user_role');
        $this->view->assign('role', $role);
        return $this->view->fetch('student_detail');
    }

    //渲染学生信息列表
    public function studentList()
    {
        //获取所有学生数据
        $studentList = StudentModel::paginate(5);

        //获取记录数量
        $count = StudentModel::count();

        $this->view->assign('studentList', $studentList);
        $this->view->assign('count', $count);
        $role = Session::get('user_role');
        $this->view->assign('role', $role);
        return $this->view->fetch('student_list');
    }

    //渲染学生编辑界面
    public function studentEdit(Request $request)
    {
        $student_id = $request->param('id');
        $result = StudentModel::get($student_id);

        $this->view->assign('title', '编辑学生');
        $this->view->assign('keywords', 'exam.com');
        $this->view->assign('desc', '在线考试系统');

        $this->view->assign('student_info', $result);

        //将班级表中所有数据赋值给当前模板
        $this->view->assign('gradeList', \app\index\model\Grade::all());

        return $this->view->fetch('student_edit');
    }

    //渲染学生个人编辑界面
    public function studentEdit2(Request $request)
    {
        $student_id = $request->param('id');
        $result = StudentModel::get($student_id);

        $this->view->assign('title', '编辑学生');
        $this->view->assign('keywords', 'exam.com');
        $this->view->assign('desc', '在线考试系统');

        $this->view->assign('student_info', $result);

        //将班级表中所有数据赋值给当前模板
        $this->view->assign('gradeList', \app\index\model\Grade::all());

        return $this->view->fetch('student_edit2');
    }

    //更新学生信息
    public function doEdit(Request $request)
    {
        //$data = $request -> param();
        $param = $request->param();

        //去掉表单中为空的数据,即没有修改的内容
        foreach ($param as $key => $value) {
            if (!empty($value)) {
                $data[$key] = $value;
            }
        }

        //设置更新条件
        $condition = ['id' => $data['id']];

        //更新当前记录
        $result = StudentModel::update($data, $condition);

        //设置返回数据
        $status = 0;
        $message = '更新失败,请检查';

        //检测更新结果,将结果返回给grade_edit模板中的ajax提交回调处理
        if (true == $result) {
            $status = 1;
            $message = '更新成功';
        }
        return ['status' => $status, 'message' => $message];
    }

    //学生修改个人信息
    public function doEdit2()
    {

        $id = $_POST["id"];
        $password = $_POST["password"];
        $email = $_POST["email"];
        $mobile = $_POST["mobile"];
        if ($password == StudentModel::where(['id' => $id])->value(['password'])) {
            $data = [
                'id' => $id,
                'email' => $email,
                'mobile' => $mobile,
            ];
        } else {
            $data = [
                'id' => $id,
                'password' => $password,
                'email' => $email,
                'mobile' => $mobile,
            ];
        }

        //设置更新条件
        $condition = ['id' => $data['id']];

        //更新当前记录
        $result = StudentModel::update($data, $condition);

        //设置返回数据
        $status = 0;
        $message = '更新失败,请检查';

        //检测更新结果,将结果返回给grade_edit模板中的ajax提交回调处理
        if (true == $result) {
            $status = 1;
            $message = '更新成功';
        }
        return ['status' => $status, 'message' => $message];
    }

    //检测学号是否可用
    public function checkStudentId(Request $request)
    {
        $userId = trim($request->param('id'));
        $status = 1;
        $message = '学号可用';
        if (StudentModel::get(['id' => $userId])) {
            //如果在表中查询到该用户名
            $status = 0;
            $message = '学号重复,请重新输入~~';
        }
        return ['status' => $status, 'message' => $message];
    }

    //检测用户邮箱是否可用
    public function checkStudentEmail(Request $request)
    {
        $userEmail = trim($request->param('email'));
        $status = 1;
        $message = '邮箱可用';
        if (StudentModel::get(['email' => $userEmail])) {
            //查询表中找到了该邮箱,修改返回值
            $status = 0;
            $message = '邮箱重复,请重新输入~~';
        }
        return ['status' => $status, 'message' => $message];
    }

    //检测用户手机号是否可用
    public function checkStudentMobile(Request $request)
    {
        $userMobile = trim($request->param('mobile'));
        $status = 1;
        $message = '手机号可用';
        if (StudentModel::get(['mobile' => $userMobile])) {
            //查询表中找到了该邮箱,修改返回值
            $status = 0;
            $message = '手机号重复,请重新输入~~';
        }
        return ['status' => $status, 'message' => $message];
    }

    //渲染学生添加界面
    public function studentAdd()
    {
        $this->view->assign('title', '添加学生');
        $this->view->assign('keywords', 'php.cn');
        $this->view->assign('desc', 'PHP中文网ThinkPHP5开发实战课程');

        //将班级表中所有数据赋值给当前模板
        $this->view->assign('gradeList', \app\index\model\Grade::all());

        return $this->view->fetch('student_add');
    }

    //添加学生到表中
    public function doAdd()
    {
        $id = $_POST["id"];
        $name = $_POST["name"];
        $password = $_POST["password"];
        $email = $_POST["email"];
        $mobile = $_POST["mobile"];
        $grade_id = $_POST["grade_id"];
        $sex = $_POST["sex"];
        $data = [
            'id' => $id,
            'name' => $name,
            'password' => $password,
            'grade_id' => $grade_id,
            'email' => $email,
            'sex' => $sex,
            'mobile' => $mobile,
        ];
        if ($data['name'] == null || $data['password'] == null || $data['email'] == null || $data['mobile'] == null) {
            //必填项不能为空
            $status = 0;
            $message = '请输入必填(*)信息';
        } else if (StudentModel::where(['id' => $data['id']])->select() || StudentModel::where(['email' => $data['email']])->select() || StudentModel::where(['mobile' => $data['mobile']])->select()) {
            //如果在表中查询到该用户名
            $status = 0;
            $message = '请输入正确信息';
        } else {
            //新增学生记录
            $result = StudentModel::create($data);

            //设置返回数据
            $status = 0;
            $message = '添加失败,请检查';

            //检测更新结果,将结果返回给grade_edit模板中的ajax提交回调处理
            if (true == $result) {
                $status = 1;
                $message = '添加成功';
            }
            db('table')->where('id', $grade_id)->setInc('num');
        }
        return ['status' => $status, 'message' => $message];
    }

    //查看成绩
    public function scoreShow()
    {
        $id = $_GET["id"];
        $scoreList = PaperUserModel::where(['user_id' => $id])->paginate(5,false,[
            'query'=>['id'=>$id],
            ]);
        $count = PaperUserModel::where(['user_id' => $id])->count();

        $this->view->assign('scoreList', $scoreList);
        $this->view->assign('count', $count);

        $role = Session::get('user_role');
        $this->view->assign('role', $role);
        
        if ($role == 3) {
            return $this->view->fetch('student_score_teacher');
        } else if ($role == 4) {
            return $this->view->fetch('student_score_student');
        } else { 
            return $this->view->fetch('student_score');
        }

    }

    //删除操作
    public function deleteStudent(Request $request)
    {
        $student_id = $request->param('id');
        StudentModel::where(['id' => $student_id])->delete();
        PaperUserModel::where(['user_id' => $student_id])->delete();
    }

}
