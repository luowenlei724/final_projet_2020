<?php
namespace app\index\controller;

use app\index\model\Blank as BlankModel;
use app\index\model\Check as CheckModel;
use app\index\model\Paper as PaperModel;
use app\index\model\PaperUser as PaperUserModel;
use app\index\model\Short as ShortModel;
use app\index\model\Tof as TofModel;
use think\Db;
use think\Request;
use think\Session;

class Paper extends Base
{
    //试卷列表
    public function paperList()
    {
        $this->view->assign('title', '试卷列表');
        $this->view->assign('keywords', '在线考试系统');
        $this->view->assign('desc', '在线考试');

        $user = Session::get('user_info');
        $paperList = PaperModel::paginate(5);
        $count = PaperModel::count();

        $this->view->assign('paperList', $paperList);
        $this->view->assign('count', $count);
        $this->view->assign('user_info', $user);

        $role = Session::get('user_role');
        $this->view->assign('role', $role);
        if ($role == 3) {
            return $this->view->fetch('paper_list_teacher');
        } else if ($role == 4) {
            return $this->view->fetch('paper_list_student');
        } else {
            return $this->view->fetch('paper_list');
        }
    }

    //试卷详情
    public function paperShow(Request $request)
    {
        $this->view->assign('title', '试卷详情');
        $this->view->assign('keywords', '在线考试系统');
        $this->view->assign('desc', '在线考试');

        $paper_id = $request->param('id');
        $paper = PaperModel::where('id', $paper_id)->find();

        $checkList = db::name('check')->where('id', 'in', $paper['check'])->select();
        $tofList = db::name('tof')->where('id', 'in', $paper['tof'])->select();
        $blankList = db::name('blank')->where('id', 'in', $paper['blank'])->select();
        $shortList = db::name('short')->where('id', 'in', $paper['short'])->select();

        $checkCount = db::name('check')->where('id', 'in', $paper['check'])->count();
        $tofCount = db::name('tof')->where('id', 'in', $paper['tof'])->count();
        $blankCount = db::name('blank')->where('id', 'in', $paper['blank'])->count();
        $shortCount = db::name('short')->where('id', 'in', $paper['short'])->count();

        $this->view->assign('title', $paper['title']); //试卷标题
        $this->view->assign('time', $paper['exam_time']); //考试时间限制

        $this->view->assign('checkList', $checkList);
        $this->view->assign('tofList', $tofList);
        $this->view->assign('blankList', $blankList);
        $this->view->assign('shortList', $shortList);

        $this->view->assign('checkCount', $checkCount);
        $this->view->assign('tofCount', $tofCount);
        $this->view->assign('blankCount', $blankCount);
        $this->view->assign('shortCount', $shortCount);

        $this->view->assign('cno1', 1); //选择题题目id
        $this->view->assign('cno2', 1); //选择题题目编号

        $this->view->assign('tno1', 1); //判断题题目id
        $this->view->assign('tno2', 1); //判断题题目编号

        $this->view->assign('bno1', 1); //填空题题目id
        $this->view->assign('bno2', 1); //填空题题目编号

        $this->view->assign('sno1', 1); //简答题题目id
        $this->view->assign('sno2', 1); //简答题题目编号

        $this->view->assign('acno1', 1); //选择题答案id

        $this->view->assign('atno1', 1); //判断题答案id

        $this->view->assign('abno1', 1); //填空题答案id

        $this->view->assign('asno1', 1); //简答题答案id

        return $this->view->fetch('paper_show');
    }

    //添加试卷页面
    public function paperAdd()
    {
        $role = Session::get('user_role');
        if ($role == 3) {
            $this->view->assign('role', $role);
            return $this->view->fetch('paper_add_teacher');
        } else {
            $this->view->assign('role', $role);
            return $this->view->fetch('paper_add');
        }
    }

    //考试页面
    public function paperExam(Request $request)
    {
        $this->view->assign('keywords', '在线考试系统');
        $this->view->assign('desc', '在线考试');

        $paper_id = $request->param('id');
        $paper = PaperModel::where('id', $paper_id)->find();

        $check = new \think\db\Expression('field(id,' . $paper['check'] . ')');
        $tof = new \think\db\Expression('field(id,' . $paper['tof'] . ')');
        $blank = new \think\db\Expression('field(id,' . $paper['blank'] . ')');
        $short = new \think\db\Expression('field(id,' . $paper['short'] . ')');

        $checkList = db::name('check')->where('id', 'in', $paper['check'])->order($check)->select();
        $tofList = db::name('tof')->where('id', 'in', $paper['tof'])->order($tof)->select();
        $blankList = db::name('blank')->where('id', 'in', $paper['blank'])->order($blank)->select();
        $shortList = db::name('short')->where('id', 'in', $paper['short'])->order($short)->select();

        $checkCount = db::name('check')->where('id', 'in', $paper['check'])->count();
        $tofCount = db::name('tof')->where('id', 'in', $paper['tof'])->count();
        $blankCount = db::name('blank')->where('id', 'in', $paper['blank'])->count();
        $shortCount = db::name('short')->where('id', 'in', $paper['short'])->count();

        $this->view->assign('title', $paper['title']); //试卷标题
        $this->view->assign('id', $paper_id); //试卷id
        $this->view->assign('time', $paper['exam_time']); //考试时间倒计时

        $timeArr = explode(":", $paper['exam_time']);
        $this->view->assign('hour', $timeArr[0]); //考试时间
        $this->view->assign('min', $timeArr[1]); //考试时间

        $this->view->assign('checkList', $checkList);
        $this->view->assign('tofList', $tofList);
        $this->view->assign('blankList', $blankList);
        $this->view->assign('shortList', $shortList);

        $this->view->assign('checkCount', $checkCount);
        $this->view->assign('tofCount', $tofCount);
        $this->view->assign('blankCount', $blankCount);
        $this->view->assign('shortCount', $shortCount);

        $this->view->assign('cno1', 1); //选择题题目id
        $this->view->assign('cno2', 1); //选择题题目编号

        $this->view->assign('tno1', 1); //判断题题目id
        $this->view->assign('tno2', 1); //判断题题目编号

        $this->view->assign('bno1', 1); //填空题题目id
        $this->view->assign('bno2', 1); //填空题题目编号

        $this->view->assign('sno1', 1); //简答题题目id
        $this->view->assign('sno2', 1); //简答题题目编号

        $this->view->assign('acno1', 1); //选择题答案id

        $this->view->assign('atno1', 1); //判断题答案id

        $this->view->assign('abno1', 1); //填空题答案id

        $this->view->assign('asno1', 1); //简答题答案id

        return $this->view->fetch('paper_exam');
    }

    //批改试卷
    public function paperScore()
    {
        $paper_id = $_POST["id"];
        $checkCount = $_POST['checkCount'];
        $tofCount = $_POST['tofCount'];
        $blankCount = $_POST['blankCount'];
        $shortCount = $_POST['shortCount'];

        $checkList = explode(',', PaperModel::where('id', $paper_id)->value('check')); //选择题题号数组
        $tofList = explode(',', PaperModel::where('id', $paper_id)->value('tof')); //判断题题号数组
        $blankList = explode(',', PaperModel::where('id', $paper_id)->value('blank')); //填空题题号数组
        $shortList = explode(',', PaperModel::where('id', $paper_id)->value('short')); //简答题题号数组

        //选择题答案
        if ($checkList) {
            $i = 0;
            while ($i < $checkCount) {
                $checkA[] = CheckModel::where('id', $checkList[$i])->value('ans');
                $i++;
            }
        }

        //判断题答案 , 题号为0判定false
        if ($tofList) {
            $i = 0;
            while ($i < $tofCount) {
                $tofA[] = tofModel::where('id', $tofList[$i])->value('ans');
                $i++;
            }
        }

        //填空题答案
        if ($blankList) {
            $i = 0;
            while ($i < $blankCount) {
                $blankA[] = blankModel::where('id', $blankList[$i])->value('ans');
                $i++;
            }

        }

        //简答题答案
        if ($shortList) {
            $i = 0;
            while ($i < $shortCount) {
                $shortA[] = shortModel::where('id', $shortList[$i])->value('ans');
                $i++;
            }
        }

        //所填选择题答案
        if ($checkList) {
            $i = 0;
            $n = 1;
            $checkArr = $_POST['canswer1'];
            while ($i < $checkCount) {
                $str = 'canswer' . $n;
                $checkAns[] = $_POST[$str];
                if ($n > 1) {$checkArr = $checkArr . ',' . $_POST[$str];}
                $i++;
                $n++;
            }
        }

        //所填判断题答案

        if ($tofList) {
            $i = 0;
            $n = 1;
            $tofArr = $_POST['tanswer1'];
            while ($i < $tofCount) {
                $str = 'tanswer' . $n;
                $tofAns[] = $_POST[$str];
                if ($n > 1) {$tofArr = $tofArr . ',' . $_POST[$str];}
                $i++;
                $n++;
            }

        }
        //所填填空题答案
        if ($blankList) {
            $i = 0;
            $n = 1;
            $blankArr = $_POST['banswer1'];
            while ($i < $blankCount) {
                $str = 'banswer' . $n;
                $blankAns[] = $_POST[$str];
                if ($n > 1) {$blankArr = $blankArr . ',' . $_POST[$str];}
                $i++;
                $n++;
            }
        }

        //所填简答题答案
        if ($shortList) {
            $i = 0;
            $n = 1;
            $shortArr = $_POST['sanswer1'];
            while ($i < $shortCount) {
                $str = 'sanswer' . $n;
                $shortAns[] = $_POST[$str];
                if ($n > 1) {$shortArr = $shortArr . ';' . $_POST[$str];}
                $i++;
                $n++;
            }
        }

        $checkscore = 0;
        $tofscore = 0;
        $blankscore = 0;
        $shortscore = 0;
        //选择题得分
        if ($checkList) {
            $i = 0;
            while ($i < $checkCount) {
                if ($checkA[$i] == $checkAns[$i]) {
                    $checkscore = $checkscore + 2;
                }
                $i++;

            }
        }

        //判断题得分
        if ($tofList) {
            $i = 0;
            while ($i < $tofCount) {
                if ($tofA[$i] == $tofAns[$i]) {
                    $tofscore = $tofscore + 2;
                }
                $i++;

            }
        }

        //填空题得分
        if ($blankList) {
            $i = 0;
            while ($i < $blankCount) {
                if ($blankA[$i] == $blankAns[$i]) {
                    $blankscore = $blankscore + 2;
                }
                $i++;

            }
        }

        //简答题得分
        if ($shortList) {
            $i = 0;
            while ($i < $shortCount) {
                $shortAList = explode(',', $shortA[$i]);
                $shortAcount = count($shortAList);
                $value = 10 / $shortAcount;
                $n = 0;
                while ($n < $shortAcount) {
                    if (stripos($shortAns[$i], $shortAList[$n]) !== false) {
                        $shortscore = $shortscore + $value;
                    }
                    $n++;
                }
                $i++;
            }
        }

        //总分
        $scorecout = $checkscore + $tofscore + $blankscore + $shortscore;

        $user_id = Session::get('user_id');
        $data = [
            'user_id' => $user_id,
            'paper_id' => $paper_id,
            'title' => $_POST["title"],
            'score' => $scorecout,
            'check' => $checkArr,
            'tof' => $tofArr,
            'blank' => $blankArr,
            'short' => $shortArr,
            'create_time' => time(),
        ];

        PaperUserModel::insert($data);

        return ['score' => $scorecout];
    }

    //渲染成绩页面
    public function paperScore2()
    {
        $scorecout = $_GET["score"];
        $this->view->assign('score', $scorecout);
        return $this->view->fetch('exam_score');
    }

    //添加试卷
    public function doAdd(Request $request)
    {
        $param = $request->param();
        $score = ($param['cnum'] + $param['tnum'] + $param['bnum']) * 2 + $param['snum'] * 10;
        $hour = $param['hour'];
        $min = $param['min'];
        if ($param['title'] == null) {
            //必填项不能为空
            $status = 0;
            $message = '请输入必填(*)信息';
        } else if ($score != 100) {
            //如果在表中查询到该用户名
            $status = 0;
            $message = '总分不为100,请输入正确题数';
        } else if ($hour == 0 && $min == 0) {
            $status = 0;
            $message = '时间限制不能为0';
        } else {
            $paper = new PaperModel();
            $check = $paper->getQuestion($param['cnum'], 'check');
            $tof = $paper->getQuestion($param['tnum'], 'tof');
            $blank = $paper->getQuestion($param['bnum'], 'blank');
            $short = $paper->getQuestion($param['snum'], 'short');

            if ($check == null) {$check = 0;}
            if ($tof == null) {$tof = 0;}
            if ($blank == null) {$blank = 0;}
            if ($short == null) {$short = 0;}

            if ($hour < 10) {$hour = '0' . $hour;}
            if ($min < 10) {$min = '0' . $min;}
            $exam_time = $hour . ':' . $min;
            $data = [
                'title' => $param['title'],
                'check' => $check,
                'tof' => $tof,
                'blank' => $blank,
                'short' => $short,
                'exam_time' => $exam_time,
            ];
            $result = PaperModel::create($data);
            if ($result == null) {
                $status = 0;
                $message = '组卷失败,请检查';
            } else {
                $status = 1;
                $message = '组卷成功';
            }
        }

        return ['status' => $status, 'message' => $message];
    }

    //删除试卷
    public function deletePaper(Request $request)
    {
        $paper_id = $request->param('id');
        PaperModel::where('id', $paper_id)->delete();
    }

}
