<?php
namespace app\index\controller;

use app\index\model\Grade;
use app\index\model\Teacher;
use think\Session;

class Index extends Base
{
    public function index()
    {
        $this -> isLogin();  //判断用户是否登录
        $this -> view -> assign('title', '在线考试系统');
        $role = Session::get('user_role');
        $this -> view -> assign('role', $role);

        return $this -> view -> fetch();  //渲染首页模板
    }
    
}
