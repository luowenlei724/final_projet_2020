<?php

namespace app\index\controller;

use app\index\model\Grade as GradeModel;
use app\index\model\Student as StudentModel;
use app\index\model\Teacher as TeacherModel;
use think\Request;
use think\Session;

class Grade extends Base
{
    //班级列表
    public function gradeList()
    {
        //获取所有班级表数据
        $grade = GradeModel::all();

        //获取记录数量
        $count = GradeModel::count();
        //遍历grade表
        foreach ($grade as $value) {
            $data = [
                'id' => $value->id,
                'num' => StudentModel::where(['grade_id'=>$value['id']])->count(),
                'update_time' => $value->update_time,
                //用关联方法teacher属性方式访问teacher表中数据
                'teacher' => isset($value->teacher->name) ? $value->teacher->name : '<span style="color:red;">未分配</span>',
            ];
            //每次关联查询结果,保存到数组$gradeList中
            $gradeList[] = $data;
        }

        $this->view->assign('gradeList', $gradeList);
        $this->view->assign('count', $count);
        $role = Session::get('user_role');
        $this -> view -> assign('role', $role);
        return $this->view->fetch('grade_list');
    }

    //班级成员查看
    public function gradeShow()
    {
        $id = $_GET["id"];
        $student = StudentModel::where(['grade_id' => $id])->paginate(5);

        $count = StudentModel::where(['grade_id' => $id])->count();

        $this->view->assign('gradeShow', $student);
        $this->view->assign('count', $count);
        $role = Session::get('user_role');
        $this -> view -> assign('role', $role);
        return $this->view->fetch('grade_show');

    }

    //渲染班级添加界面
    public function gradeAdd()
    {
        //给模板赋值seo变量
        $this->view->assign('title', '编辑班级');
        $this->view->assign('keywords', 'exam.com');
        $this->view->assign('desc', '在线考试系统');

        //渲染添加模板
        return $this->view->fetch('grade_add');
    }

    //添加班级
    public function doAdd(Request $request)
    {
        //从提交表单中获取数据
        $data = $request->param();
        $data1 = [
            'id' => $data['id'],
            'update_time' => time(),
        ];
        $data2 = [
            'grade_id' => $data['id'],
        ];
        $condition = [
            'id' => $data['name'],
        ];

        //更新当前记录
        $result = GradeModel::insert($data1);
        if ($data['name']) {
            TeacherModel::update($data2, $condition);
        }

        //设置返回数据的初始值
        $status = 0;
        $message = '添加失败,请检查';

        //检测更新结果,将结果返回给grade_edit模板中的ajax提交回调处理
        if (true == $result) {
            $status = 1;
            $message = '添加成功';
        }

        //自动转为json格式返回
        return ['status' => $status, 'message' => $message];
    }

    //删除操作
    public function deleteGrade(Request $request)
    {
        $id = $request->param('id');
        GradeModel::where('id', $id)->delete();
        $data = [
            'grade_id' => null,
        ];
        $condition = [
            'grade_id' => $id,
        ];
        StudentModel::update($data, $condition);
        TeacherModel::update($data, $condition);
    }

}
