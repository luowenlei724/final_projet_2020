<?php
namespace app\index\controller;

use app\index\model\Blank as BlankModel;
use app\index\model\Check as CheckModel;
use app\index\model\Short as ShortModel;
use app\index\model\Tof as TofModel;
use think\Request;
use think\session;

class Question extends Base
{

    //选择题列表
    public function checkList()
    {
        $role = Session::get('user_role');

        $this->view->assign('title', '题库');
        $this->view->assign('keywords', '在线考试系统');
        $this->view->assign('desc', '在线考试');

        $user = Session::get('user_info');
        $questionList = CheckModel::paginate(5);
        $count = CheckModel::count();

        $this->view->assign('questionList', $questionList);
        $this->view->assign('count', $count);
        $this->view->assign('user_info', $user);

        $this->view->assign('role', $role);
        return $this->view->fetch('check_list');

    }

    //判断题列表
    public function tofList()
    {
        $this->view->assign('title', '题库');
        $this->view->assign('keywords', '在线考试系统');
        $this->view->assign('desc', '在线考试');

        $user = Session::get('user_info');
        $questionList = TofModel::paginate(5);
        $count = TofModel::count();

        $this->view->assign('questionList', $questionList);
        $this->view->assign('count', $count);
        $this->view->assign('user_info', $user);

        $role = Session::get('user_role');

        $this->view->assign('role', $role);
        return $this->view->fetch('tof_list');

    }

    //填空题列表
    public function blankList()
    {
        $this->view->assign('title', '题库');
        $this->view->assign('keywords', '在线考试系统');
        $this->view->assign('desc', '在线考试');

        $user = Session::get('user_info');
        $questionList = BlankModel::paginate(5);
        $count = BlankModel::count();

        $this->view->assign('questionList', $questionList);
        $this->view->assign('count', $count);
        $this->view->assign('user_info', $user);
        $role = Session::get('user_role');

        $this->view->assign('role', $role);
        return $this->view->fetch('blank_list');

    }

    //简答题列表
    public function shortList()
    {
        $this->view->assign('title', '题库');
        $this->view->assign('keywords', '在线考试系统');
        $this->view->assign('desc', '在线考试');

        $user = Session::get('user_info');
        $questionList = ShortModel::paginate(5);
        $count = ShortModel::count();

        $this->view->assign('questionList', $questionList);
        $this->view->assign('count', $count);
        $this->view->assign('user_info', $user);
        $role = Session::get('user_role');

        $this->view->assign('role', $role);
        return $this->view->fetch('short_list');

    }

    //渲染添加选择题
    public function checkAdd()
    {
        $this->view->assign('title', '添加题目');
        $this->view->assign('keywords', 'exam.com');
        $this->view->assign('desc', '在线考试系统');
        return $this->view->fetch('check_add');
    }

    //添加选择题
    public function checkDoAdd()
    {
        $status = 1;
        $message = '添加成功';
        $data = [
            'content' => $_POST['content'],
            'op1' => $_POST['op1'],
            'op2' => $_POST['op2'],
            'op3' => $_POST['op3'],
            'op4' => $_POST['op4'],
            'ans' => $_POST['ans'],
        ];
        if ($data['content'] == null || $data['op1'] == null || $data['op2'] == null || $data['op3'] == null || $data['op4'] == null) {
            //必填项不能为空
            $status = 0;
            $message = '请输入必填(*)信息';
        } else {
            $result = CheckModel::insert($data);

            if ($result === null) {
                $status = 0;
                $message = '添加失败';
            }
        }
        return ['status' => $status, 'message' => $message];
    }

    //渲染添加判断题
    public function tofAdd()
    {
        $this->view->assign('title', '添加题目');
        $this->view->assign('keywords', 'exam.com');
        $this->view->assign('desc', '在线考试系统');
        return $this->view->fetch('tof_add');
    }

    //添加判断题
    public function tofDoAdd()
    {
        $status = 1;
        $message = '添加成功';
        $data = [
            'content' => $_POST['content'],
            'ans' => $_POST['ans'],
        ];
        if ($data['content'] == null) {
            //必填项不能为空
            $status = 0;
            $message = '请输入必填(*)信息';
        } else {
            $result = TofModel::insert($data);

            if ($result === null) {
                $status = 0;
                $message = '添加失败';
            }
        }
        return ['status' => $status, 'message' => $message];
    }

    //渲染添加填空题
    public function blankAdd()
    {
        $this->view->assign('title', '添加题目');
        $this->view->assign('keywords', 'exam.com');
        $this->view->assign('desc', '在线考试系统');
        return $this->view->fetch('blank_add');
    }

    //添加填空题
    public function blankDoAdd()
    {
        $status = 1;
        $message = '添加成功';
        $data = [
            'content' => $_POST['content'],
            'ans' => $_POST['ans'],
        ];
        if ($data['content'] == null || $data['ans'] == null) {
            //必填项不能为空
            $status = 0;
            $message = '请输入必填(*)信息';
        } else {
            $result = BlankModel::insert($data);

            if ($result === null) {
                $status = 0;
                $message = '添加失败';
            }
        }
        return ['status' => $status, 'message' => $message];
    }

    //渲染添加简答题
    public function shortAdd()
    {
        $this->view->assign('title', '添加题目');
        $this->view->assign('keywords', 'exam.com');
        $this->view->assign('desc', '在线考试系统');
        return $this->view->fetch('short_add');
    }

    //添加简答题
    public function shortDoAdd()
    {
        $status = 1;
        $message = '添加成功';
        $data = [
            'content' => $_POST['content'],
            'ans' => str_replace('，', ',', $_POST['ans']),
        ];
        if ($data['content'] == null || $data['ans'] == null) {
            //必填项不能为空
            $status = 0;
            $message = '请输入必填(*)信息';
        } else {
            $result = ShortModel::insert($data);

            if ($result === null) {
                $status = 0;
                $message = '添加失败';
            }
        }
        return ['status' => $status, 'message' => $message];
    }

    //渲染编辑选择题
    public function checkEdit(Request $request)
    {
        $id = $request->param('id');
        $result = CheckModel::get($id);
        $this->view->assign('check', $result->getData());
        return $this->view->fetch('check_edit');
    }

    //渲染编辑判断题
    public function tofEdit(Request $request)
    {
        $id = $request->param('id');
        $result = TofModel::get($id);
        $this->view->assign('tof', $result->getData());
        return $this->view->fetch('tof_edit');
    }

    //渲染编辑填空题
    public function blankEdit(Request $request)
    {
        $id = $request->param('id');
        $result = BlankModel::get($id);
        $this->view->assign('blank', $result->getData());
        return $this->view->fetch('blank_edit');
    }

    //渲染编辑简答题
    public function shortEdit(Request $request)
    {
        $id = $request->param('id');
        $result = ShortModel::get($id);
        $this->view->assign('short', $result->getData());
        return $this->view->fetch('short_edit');
    }

    //编辑选择题
    public function checkDoEdit(Request $request)
    {
        $param = $request->param();

        //去掉表单中为空的数据,即没有修改的内容
        foreach ($param as $key => $value) {
            if (!empty($value)) {
                $data[$key] = $value;
            }
        }

        $condition = ['id' => $data['id']];
        $result = CheckModel::update($data, $condition);
        if (true == $result) {
            return ['status' => 1, 'message' => '更新成功'];
        } else {
            return ['status' => 0, 'message' => '更新失败,请检查'];
        }
    }

    //编辑判断题
    public function tofDoEdit(Request $request)
    {
        $param = $request->param();

        //去掉表单中为空的数据,即没有修改的内容
        foreach ($param as $key => $value) {
            if (!empty($value)) {
                $data[$key] = $value;
            }
        }

        $condition = ['id' => $data['id']];
        $result = TofModel::update($data, $condition);
        if (true == $result) {
            return ['status' => 1, 'message' => '更新成功'];
        } else {
            return ['status' => 0, 'message' => '更新失败,请检查'];
        }
    }

    //编辑填空题
    public function blankDoEdit(Request $request)
    {
        $param = $request->param();

        //去掉表单中为空的数据,即没有修改的内容
        foreach ($param as $key => $value) {
            if (!empty($value)) {
                $data[$key] = $value;
            }
        }

        $condition = ['id' => $data['id']];
        $result = BlankModel::update($data, $condition);
        if (true == $result) {
            return ['status' => 1, 'message' => '更新成功'];
        } else {
            return ['status' => 0, 'message' => '更新失败,请检查'];
        }
    }

    //编辑简答题
    public function shortDoEdit(Request $request)
    {
        $param = $request->param();

        //去掉表单中为空的数据,即没有修改的内容
        foreach ($param as $key => $value) {
            if (!empty($value)) {
                $data[$key] = $value;
            }
        }

        $condition = ['id' => $data['id']];
        $result = ShortModel::update($data, $condition);
        if (true == $result) {
            return ['status' => 1, 'message' => '更新成功'];
        } else {
            return ['status' => 0, 'message' => '更新失败,请检查'];
        }
    }

    //删除选择题
    public function deleteCheck(Request $request)
    {
        $question_id = $request->param('id');
        CheckModel::where('id', $question_id)->delete();
    }

    //删除判断题
    public function deleteTof(Request $request)
    {
        $question_id = $request->param('id');
        TofModel::where('id', $question_id)->delete();
    }

    //删除填空题
    public function deleteBlank(Request $request)
    {
        $question_id = $request->param('id');
        BlankModel::where('id', $question_id)->delete();
    }

    //删除简答题
    public function deleteShort(Request $request)
    {
        $question_id = $request->param('id');
        ShortModel::where('id', $question_id)->delete();
    }
}
