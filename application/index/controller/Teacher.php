<?php

namespace app\index\controller;

use app\index\model\Grade as GradeModel;
use app\index\model\Student as StudentModel;
use app\index\model\Teacher as TeacherModel;
use think\Request;
use think\Session;

class Teacher extends Base
{

    //个人信息
    public function teacherDetail()
    {
        $userName = Session::get('user_id');

        //获取所有学生数据
        $teacherList = TeacherModel::where(['id' => $userName])->select();

        $this->view->assign('teacherList', $teacherList);

        //设置当前页面的seo模板变量
        $this->view->assign('title', '个人信息');
        $this->view->assign('keywords', 'exam.com');
        $this->view->assign('desc', '在线考试系统');

        return $this->view->fetch('teacher_detail');
    }

    //渲染学生信息列表
    public function studentList()
    {
        $user = Session::get('user_info');
        if ($user['grade_id'] == null) {
            $studentList = null;
            $count = 0;
        } else {
            $studentList = StudentModel::where(['grade_id' => $user['grade_id']])->select();
            if ($studentList == null) {
                $count = 0;
                $studentList = '0';
            } else {

                //获取所有学生数据
                $studentList = StudentModel::where(['grade_id' => $user['grade_id']])->paginate(5);
                //获取记录数量
                $count = StudentModel::where(['grade_id' => $user['grade_id']])->count();
            }
        }
        $this->view->assign('studentList', $studentList);
        $this->view->assign('count', $count);

        return $this->view->fetch('student_list');
    }
    //教师列表
    public function teacherList()
    {
        //获取所有教师表teacher数据
        $teacherList = TeacherModel::paginate(5);

        //获取记录数量
        $count = TeacherModel::count();
        //遍历teacher表

        $this->view->assign('teacherList', $teacherList);
        $this->view->assign('count', $count);

        //设置当前页面的seo模板变量
        $this->view->assign('title', '教师列表');
        $this->view->assign('keywords', 'exam.com');
        $this->view->assign('desc', '在线考试系统');
        $role = Session::get('user_role');
        $this -> view -> assign('role', $role);
        return $this->view->fetch('teacher_list');
    }

    //班级列表
    public function gradeList()
    {
        $user = Session::get('user_info');
        $grade = GradeModel::where(['id' => $user['grade_id']])->find();
        // select查询出的是多条数据，需要在模版volist循环打印出来
        // find和get获取单条数据，可直接在模版打印；
        if ($grade == null) {
            $data = null;
        } else {

            $data = [
                'id' => $grade['id'],
                'num' => StudentModel::where(['grade_id' => $grade['id']])->count(),
                'update_time' => $grade['update_time'],
                //用关联方法teacher属性方式访问teacher表中数据
                'teacher' => $user['name'],
            ];
        }

        $this->view->assign('gradeList', $data);
        $role = Session::get('user_role');
        $this -> view -> assign('role', $role);
        return $this->view->fetch('grade_list');
    }

    //班级成员查看
    public function gradeShow()
    {
        $id = $_GET["id"];
        $student = StudentModel::where(['grade_id' => $id])->paginate(5);

        $count = StudentModel::where(['grade_id' => $id])->count();

        $this->view->assign('gradeShow', $student);
        $this->view->assign('count', $count);
        $role = Session::get('user_role');
        $this -> view -> assign('role', $role);
        return $this->view->fetch('grade_show');

    }

    //渲染教师编辑界面
    public function teacherEdit(Request $request)
    {
        $teacher_id = $request->param('id');
        $result = TeacherModel::get($teacher_id);

        //设置当前页面的seo模板变量
        $this->view->assign('title', '编辑教师');
        $this->view->assign('keywords', 'exam.com');
        $this->view->assign('desc', '在线考试系统');

        //给当前教师编辑页面模板赋值
        $this->view->assign('teacher_info', $result);

        //将班级表中所有数据赋值给当前模板
        $this->view->assign('gradeList', \app\index\model\Grade::all());

        //渲染出当前的编辑模板
        return $this->view->fetch('teacher_edit');
    }

    //渲染教师个人编辑界面
    public function teacherEdit2(Request $request)
    {
        $teacher_id = $request->param('id');
        $result = TeacherModel::get($teacher_id);

        //设置当前页面的seo模板变量
        $this->view->assign('title', '编辑教师');
        $this->view->assign('keywords', 'exam.com');
        $this->view->assign('desc', '在线考试系统');

        //给当前教师编辑页面模板赋值
        $this->view->assign('teacher_info', $result);

        //将班级表中所有数据赋值给当前模板
        $this->view->assign('gradeList', \app\index\model\Grade::all());

        //渲染出当前的编辑模板
        return $this->view->fetch('teacher_edit2');
    }

    //教师更新
    public function doEdit(Request $request)
    {

        //$data = $request -> param();
        $param = $request->param();

        //去掉表单中为空的数据,即没有修改的内容
        foreach ($param as $key => $value) {
            if (!empty($value)) {
                $data[$key] = $value;
            }
        }

        //设置更新条件
        $condition = ['id' => $data['id']];

        //更新当前记录
        $result = TeacherModel::update($data, $condition);

        //设置返回数据
        $status = 0;
        $message = '更新失败,请检查';

        //检测更新结果,将结果返回给grade_edit模板中的ajax提交回调处理
        if (true == $result) {
            $status = 1;
            $message = '更新成功';
        }
        return ['status' => $status, 'message' => $message];
    }

    //教师修改个人信息
    public function doEdit2()
    {

        $id = $_POST["id"];
        $password = $_POST["password"];
        $email = $_POST["email"];
        $mobile = $_POST["mobile"];
        if ($password == TeacherModel::where(['id' => $id])->value(['password'])) {
            $data = [
                'id' => $id,
                'email' => $email,
                'mobile' => $mobile,
            ];
        } else {
            $data = [
                'id' => $id,
                'password' => $password,
                'email' => $email,
                'mobile' => $mobile,
            ];
        }

        //设置更新条件
        $condition = ['id' => $data['id']];

        //更新当前记录
        $result = TeacherModel::update($data, $condition);

        //设置返回数据
        $status = 0;
        $message = '更新失败,请检查';

        //检测更新结果,将结果返回给grade_edit模板中的ajax提交回调处理
        if (true == $result) {
            $status = 1;
            $message = '更新成功';
        }
        return ['status' => $status, 'message' => $message];
    }

    //渲染教师添加界面
    public function teacherAdd()
    {
        $this->view->assign('title', '添加教师');
        $this->view->assign('keywords', 'exam.com');
        $this->view->assign('desc', '在线考试系统');

        //将班级表中所有数据赋值给当前模板
        $this->view->assign('gradeList', \app\index\model\Grade::all());

        return $this->view->fetch('teacher_add');
    }

    //检测工号是否可用
    public function checkTeacherId(Request $request)
    {
        $userId = trim($request->param('id'));
        $status = 1;
        $message = '工号可用';
        if (TeacherModel::get(['id' => $userId])) {
            //如果在表中查询到该用户名
            $status = 0;
            $message = '工号重复,请重新输入~~';
        }
        return ['status' => $status, 'message' => $message];
    }

    //检测用户邮箱是否可用
    public function checkTeacherEmail(Request $request)
    {
        $userEmail = trim($request->param('email'));
        $status = 1;
        $message = '邮箱可用';
        if (TeacherModel::get(['email' => $userEmail])) {
            //查询表中找到了该邮箱,修改返回值
            $status = 0;
            $message = '邮箱重复,请重新输入~~';
        }
        return ['status' => $status, 'message' => $message];
    }
    //检测用户手机号是否可用
    public function checkTeacherMobile(Request $request)
    {
        $userMobile = trim($request->param('mobile'));
        $status = 1;
        $message = '手机号可用';
        if (TeacherModel::get(['mobile' => $userMobile])) {
            //查询表中找到了该邮箱,修改返回值
            $status = 0;
            $message = '手机号重复,请重新输入~~';
        }
        return ['status' => $status, 'message' => $message];
    }

    //添加教师
    public function doAdd()
    {
        $id = $_POST["id"];
        $name = $_POST["name"];
        $password = $_POST["password"];
        $email = $_POST["email"];
        $mobile = $_POST["mobile"];
        $grade_id = $_POST["grade_id"];
        $sex = $_POST["sex"];
        $data = [
            'id' => $id,
            'name' => $name,
            'password' => $password,
            'grade_id' => $grade_id,
            'email' => $email,
            'sex' => $sex,
            'mobile' => $mobile,
        ];
        if ($data['name'] == null || $data['password'] == null || $data['email'] == null || $data['mobile'] == null) {
            //必填项不能为空
            $status = 0;
            $message = '请输入必填(*)信息';
        } else if (TeacherModel::where(['id' => $data['id']])->select() || TeacherModel::where(['email' => $data['email']])->select() || TeacherModel::where(['mobile' => $data['mobile']])->select()) {
            //如果在表中查询到该用户名
            $status = 0;
            $message = '请输入正确信息';
        } else {

            $result = TeacherModel::create($data);

            //设置返回数据
            $status = 0;
            $message = '添加失败,请检查';

            //检测更新结果,将结果返回给teacher_add模板中的ajax提交回调处理
            if (true == $result) {
                $status = 1;
                $message = '添加成功';
            }
        }
        return ['status' => $status, 'message' => $message];

    }

    //删除操作
    public function deleteTeacher(Request $request)
    {
        $teacher_id = $request->param('id');
        TeacherModel::where(['id' => $teacher_id])->delete();
    }

    //渲染添加学生到班级界面
    public function studentAdd(Request $request)
    {

        //设置当前页面的seo模板变量
        $this->view->assign('title', '添加学生');
        $this->view->assign('keywords', 'exam.com');
        $this->view->assign('desc', '在线考试系统');

        //渲染出当前的编辑模板
        return $this->view->fetch('student_add');
    }

    //添加学生到班级中,更新班级号 
    public function studentDoAdd()
    {
        $user = Session::get('user_info');
        $grade_id = [
            'grade_id' => $user['grade_id'],
        ];
        $id = $_POST["id"];
        $data = [
            'id' => $id,
        ];
        if ($data['id'] == null) {
            //必填项不能为空
            $status = 0;
            $message = '请输入学生学号';
        } else if (StudentModel::where(['id' => $data['id']])->where('grade_id', 'NEQ', '')->select()) {
            //如果在表中查询到该学生已分配班级
            $status = 0;
            $message = '该学生已被分配在其他班级';
        } else {
            //新增学生记录
            $result = StudentModel::update($grade_id, $data);

            //设置返回数据
            $status = 0;
            $message = '添加失败,请检查';

            //检测更新结果,将结果返回给grade_edit模板中的ajax提交回调处理
            if (true == $result) {
                $status = 1;
                $message = '添加成功';
            }
        }
        return ['status' => $status, 'message' => $message];
    }

    //删除操作,将学生移出班级,使班级号=null  
    public function deleteStudent(Request $request)
    {
        $user = Session::get('user_info');
        $grade_id = [
            'grade_id' => $user['grade_id'],
        ];
        $student_id = $request->param('id');
        StudentModel::update(['grade_id' => ''], ['id' => $student_id]);
        echo '$num';
    }

}
