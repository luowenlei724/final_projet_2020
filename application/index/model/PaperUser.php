<?php

namespace app\index\model;

use think\Model;

class PaperUser extends Model
{

    // 更新自动完成列表
    protected $update = [];
    // 是否需要自动写入时间戳 如果设置为字符串 则表示时间字段的类型
    protected $autoWriteTimestamp = true; //自动写入
    // 创建时间字段
    protected $createTime = 'create_time';

    // 时间字段取出后的默认时间格式
    protected $dateFormat = 'Y年m月d日';

    //更新时间获取器
    public function getCreateTimeAttr($value)
    {
        return date('Y/m/d H:i', $value);
    }

}
