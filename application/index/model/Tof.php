<?php

namespace app\index\model;

use think\Model;

class Tof  extends Model
{


   // 更新自动完成列表
   protected $update = [];
 
   public function getRoleAttr($value)
   {
    $role = [
        1 => '选择题',
        2 => '判断题',
        3 => '填空题',
        4 => '简答题',
    ];
    return $role[$value];
   }

   public function getAnsAttr($value)
   {
    $ans = [
        1 => '正确',
        2 => '错误',
    ];
    return $ans[$value];
   }

 

}