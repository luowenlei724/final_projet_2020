<?php

namespace app\index\model;

use think\db;
use think\Model;

class Paper extends Model
{

    // 更新自动完成列表
    protected $update = [];
    // 是否需要自动写入时间戳 如果设置为字符串 则表示时间字段的类型
    protected $autoWriteTimestamp = true; //自动写入
    // 创建时间字段
    protected $createTime = 'create_time';
    // 更新时间字段
    protected $updateTime = 'update_time';
    // 时间字段取出后的默认时间格式
    protected $dateFormat = 'Y年m月d日';

    public function getRoleAttr($value)
    {
        $role = [
            1 => '选择题',
            2 => '判断题',
            3 => '填空题',
            4 => '简答题',
        ];
        return $role[$value];
    }

    //抽取题目
    public function getQuestion($num, $table)
    {
        if ($num == 0) {return '';} else {

            $count = Db::name($table)->count(); //获取总记录数
            $max = Db::name($table)->max('id');
            $min = Db::name($table)->min('id'); //统计某个字段最小数据
            if ($count < $num) {$num = $count;}
            $i = 1;
            $flag = 0;
            $id = Db::name($table)->column('id');
            $flag2 = implode(',', $id);
            $ary = array();
            while ($i <= $num) {
                $rundnum = rand($min, $max); //抽取随机数
                if ($flag != $rundnum) {
                    if (stripos($flag2, (string)$rundnum) !== false) {
                        //过滤重复
                        if (!in_array($rundnum, $ary)) {
                            $ary[] = $rundnum;
                            $flag = $rundnum;
                        } else {
                            $i--;
                        }
                        $i++;
                    }
                }
            }
            return implode(',', $ary);
        }
    }

    //更新时间获取器
    public function getCreateTimeAttr($value)
    {
        return date('Y/m/d H:i', $value);
    }

}
