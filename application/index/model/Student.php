<?php

namespace app\index\model;

use think\Model;

class Student  extends Model
{


   // 更新自动完成列表
   protected $update = [];
   // 是否需要自动写入时间戳 如果设置为字符串 则表示时间字段的类型
   protected $autoWriteTimestamp = true; //自动写入
   // 创建时间字段
   protected $createTime = 'create_time';
   // 更新时间字段
   protected $updateTime = 'update_time';
   // 时间字段取出后的默认时间格式
   protected $dateFormat = 'Y年m月d日';

   //数据表中角色字段:role返回值处理
   public function getRoleAttr($value)
   {
       $role = [
           0 => '超级管理员',
           1 => '超级管理员',
           2 => '管理员',
           3 => '教师',
           4 => '学生',
       ];
       return $role[$value];
   }

   //数据表中角色字段:sex返回值处理
   public function getSexAttr($value)
   {
       $sex = [
           1 => '男',
           2 => '女',
       ];
       return $sex[$value];
   }

   //密码修改器
   public function setPasswordAttr($value)
   {
       return md5($value);
   }

   //更新时间获取器
   public function getUpdateTimeAttr($value)
   {
       return date('Y/m/d H:i', $value);
   }

}