<?php

namespace app\index\model;


use think\Model;

class Grade extends Model
{

    //设置当前表默认日期时间显示格式
    protected $dateFormat = 'Y/m/d';

    // 开启自动写入时间戳
    protected $autoWriteTimestamp = true;

    protected $createTime = 'create_time';

    protected $updateTime = 'update_time';

    // 定义关联方法
    public function teacher()
    {
        // 班级表与教师表是1对1关联
        return $this->hasOne('teacher');
    }

    // 定义关联方法
    public function student()
    {
        return $this->hasMany('student');
    }




}