/*
SQLyog Ultimate v12.5.0 (64 bit)
MySQL - 5.7.26 : Database - exam
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`exam` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `exam`;

/*Table structure for table `blank` */

DROP TABLE IF EXISTS `blank`;

CREATE TABLE `blank` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '题号',
  `role` tinyint(2) unsigned NOT NULL DEFAULT '3' COMMENT '1选择题 2判断题 3填空题 4简单题',
  `content` text NOT NULL COMMENT '题目详情',
  `ans` text NOT NULL COMMENT '答案',
  `value` int(2) NOT NULL DEFAULT '2' COMMENT '分值',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/*Data for the table `blank` */

LOCK TABLES `blank` WRITE;

insert  into `blank`(`id`,`role`,`content`,`ans`,`value`) values 
(1,3,'_____程序是将计算机高级语言源程序翻译成目标程序的系统软件。','编译',2),
(2,3,'计算机内存储器分为ROM和RAM，其中存放在RAM上的信息将随着断电而消失，因此在关机前，应把信息先存入_____','外存',2),
(3,3,'计算机内进行算术与逻辑运算的功能部件是_____','运算器',2),
(4,3,'著名数学家冯•诺依曼（von Neumann）提出了_____和程序控制理论。','程序存储',2),
(5,3,'“N”的ASCII码为4EH，由此可推算出ASCII码为01001010B所对应的字符是_____','J',2),
(6,3,'描述信息存储容量的单位1GB＝_____KB','1048576',2),
(7,3,'在I/O设备中，显示器是计算机的_____设备。','输出',2),
(8,3,' 在计算机系统中扩展名为.TXT的文件是_____文件。','文本',2),
(9,3,'在域名系统中，中国的域名是_____字符','cn',2),
(10,3,'在Excel数据库中，提供了两种筛选清单命令，分别是高级筛选和_____','自动筛选',2);

UNLOCK TABLES;

/*Table structure for table `check` */

DROP TABLE IF EXISTS `check`;

CREATE TABLE `check` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '题号',
  `role` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '1选择题 2判断题 3填空题 4简单题',
  `content` text NOT NULL COMMENT '题目详情',
  `op1` text NOT NULL COMMENT '选项1',
  `op2` text NOT NULL COMMENT '选项2',
  `op3` text NOT NULL COMMENT '选项3',
  `op4` text NOT NULL COMMENT '选项4',
  `ans` int(1) NOT NULL COMMENT '答案',
  `value` int(2) NOT NULL DEFAULT '2' COMMENT '分值',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

/*Data for the table `check` */

LOCK TABLES `check` WRITE;

insert  into `check`(`id`,`role`,`content`,`op1`,`op2`,`op3`,`op4`,`ans`,`value`) values 
(1,1,'计算机中数据的表示形式是','八进制','十进制','二进制','十六进制',3,2),
(2,1,'计算机硬件能直接识别和执行的只有','高级语言','符号语言','汇编语言','机器语言',4,2),
(3,1,'具有多媒体功能的微型计算机系统中，常用的CD-ROM是','只读型大容量软盘','只读型光盘','只读型硬盘','半导体只读存储器',2,2),
(4,1,'把WINDOWS的窗口和对话框作一比较，窗口可以移动和改变大小，而对话框','既不能移动，也不能改变大小','仅可以移动，不能改变大小','仅可以改变大小，不能移动','既能移动，也能改变大小',2,2),
(5,1,'在WINDOWS中，“任务栏”的作用是','显示系统的所有功能','只显示当前活动窗口名','只显示正在后台工作的窗口名','实现窗口之间的切换',4,2),
(6,1,'在word的编辑状态，执行编辑菜单中“复制”命令后','将选择的内容复制到插入点处','将选择的内容复制到剪贴板','插入点所在段落内容被复制到剪贴板','光标所在段落内容被复制到剪贴板',2,2),
(7,1,'在word的编辑状态，进行字体设置操作后，按新设置的字体显示的文字是','插入点所在段落中的文字','文档中被选择的文字','插入点所在行中的文字','文档的全部文字',2,2),
(8,1,'下列四个不同数字表示的数中，数值最大的是','二进制数11011101','八进制数334','十进制数219','十六进制数DA',1,2),
(9,1,'设WINDOWS桌面上已经有某应用程序的图标，要运行该程序，可以','用鼠标左键单击该图标','用鼠标右键单击该图标','用鼠标左键双击该图标','用鼠标右键双击该图标',3,2),
(10,1,'在WINDOWS的“资源管理器”窗口中，如果想一次选定多个分散的文件或文件夹，正确的操作是','按住CTRL键，用鼠标右键逐个选取。','按住CTRL键，用鼠标左键逐个选取。','按住SHIFT键，用鼠标右键逐个选取。','按住SHIFT键，用鼠标左键逐个选取。',2,2);

UNLOCK TABLES;

/*Table structure for table `grade` */

DROP TABLE IF EXISTS `grade`;

CREATE TABLE `grade` (
  `id` varchar(11) NOT NULL COMMENT '班级号',
  `update_time` int(11) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `grade` */

LOCK TABLES `grade` WRITE;

insert  into `grade`(`id`,`update_time`) values 
('1621805',1583826729),
('1621806',1583826729),
('1621807',1584172290),
('1621808',1584172290);

UNLOCK TABLES;

/*Table structure for table `paper` */

DROP TABLE IF EXISTS `paper`;

CREATE TABLE `paper` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(50) DEFAULT NULL COMMENT '试卷标题',
  `check` text COMMENT '选择题号',
  `tof` text COMMENT '判断题号',
  `blank` text COMMENT '填空题号',
  `short` text COMMENT '简答题号',
  `exam_time` varchar(11) DEFAULT '01:40' COMMENT '考试时间',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

/*Data for the table `paper` */

LOCK TABLES `paper` WRITE;

insert  into `paper`(`id`,`title`,`check`,`tof`,`blank`,`short`,`exam_time`,`create_time`,`update_time`) values 
(1,'测试试卷','5,1,7,6,9,4,2,3,10,8','0','2,4,6,8,9,10','1,3,4','01:40',1584606643,1584606643),
(2,'测试试卷2','1,2,3,5,6,8,10','1,4,5,6,8,10','2,4,6,8,9,10','1,3,4','01:40',1584606643,1584606643),
(3,'测试试卷3','1,2,3,5,6,8,10','1,4,5,6,8,10','2,4,6,8,9,10','1,3,4','01:40',1584606643,1584606643),
(9,'正式测试','5,1,7,6,9,4,2,3,10,8','12,11,10,5,9,20,16,3,14,2','7,6,9,10,3,1,2,8,5,4','3,2,4,1','01:10',1584876496,1584876496),
(12,'简答题','0','0','0','3,2,4,1','00:01',1585162973,1585162973),
(14,'选择题','5,6,8,2,9,10,3,1,7,4','0','0','0','01:00',1585203050,1585203050),
(15,'判断题','0','10,3,8,15,17,13,4,16,6,11,19,7,1,12,14,2,9,18,20,5','0','0','01:00',1585203064,1585203064),
(16,'填空题','0','0','9,2,4,6,10,7,5,3,8,1','0','01:00',1585203073,1585203073),
(17,'最终测试','6,4,2,5,10,3,7,8,9,1','10,6,8,17,20,14,12,11,16,15','1,6,10,3,7,2,8,5,9,4','4,6,5,2','01:00',1587277262,1587277262);

UNLOCK TABLES;

/*Table structure for table `paper_user` */

DROP TABLE IF EXISTS `paper_user`;

CREATE TABLE `paper_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` varchar(11) DEFAULT NULL COMMENT '考生学号',
  `paper_id` varchar(11) DEFAULT NULL COMMENT '试卷id',
  `title` varchar(50) DEFAULT NULL COMMENT '试卷标题',
  `score` int(3) DEFAULT NULL COMMENT '成绩',
  `check` text COMMENT '所填单选题答案',
  `tof` text COMMENT '所填判断题答案',
  `blank` text COMMENT '所填填空题答案',
  `short` text COMMENT '所填简答题答案',
  `create_time` int(11) DEFAULT NULL COMMENT '考试时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

/*Data for the table `paper_user` */

LOCK TABLES `paper_user` WRITE;

insert  into `paper_user`(`id`,`user_id`,`paper_id`,`title`,`score`,`check`,`tof`,`blank`,`short`,`create_time`) values 
(1,'201601','1','测试试卷',80,'1,2,3,4,1,2,3,4','1,2,1,2,1,2','真的,对的,好的,不错,OK,123','我怎么知道你在说什么,好的我知道了别说了,真的吗,哈哈哈哈;\r\n嘿嘿嘿;\r\n哈哈哈哈\r\n我知道了别说了;',1584876496),
(2,'000','9','正式测试',11,'4,3,2,2,0,0,0,0,0,0','0,0,0,0,0,0,0,0,0,0',',,,,,编译,,,,',';;;硬件系统',1585158069),
(3,'000','9','正式测试',2,'4,0,0,0,0,0,0,0,0,0','0,0,0,0,0,0,0,0,0,0',',,,,,,,,,','asdafas;噶尔ad;嗯嗯嗯嗯呃;啊,啊,啊,啊喔喔',1585158303),
(4,'000','9','正式测试',8,'4,3,2,2,0,0,0,0,0,0','0,0,0,0,0,0,0,0,0,0',',,,,,,,,,',';;;',1585160096),
(5,'000','9','正式测试',4,'4,0,0,0,0,0,0,0,0,0','0,0,0,0,0,0,0,0,1,0',',,,,,,,,,',';;;',1585160246),
(6,'000','9','正式测试',8,'4,3,2,2,0,0,0,0,0,0','0,0,0,0,0,0,0,0,0,0',',,,,,,,,,',';;;',1585161941),
(7,'000','9','正式测试',4,'4,3,3,0,0,0,0,0,0,0','0,0,0,0,0,0,0,0,0,0',',,,,,,,,,',';;;',1585163399),
(8,'000','2','测试试卷2',2,'0,4,3,0,0,0,0','0,0,0,0,0,0',',,,,,',';;',1585163427),
(9,'000','9','正式测试',2,'0,0,0,0,0,0,0,0,0,0','1,0,0,0,0,0,0,0,0,0',',,,,,,,,,',';;;',1585193599),
(10,'000','1','测试试卷',2,'4,0,0,0,0,0,0,0,0,0','5',',,,,,',';;',1585196969),
(11,'000','14','选择题',2,'4,5,5,5,5,5,5,5,5,5','5','','',1585203119),
(12,'000','15','判断题',2,'5','1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0','','',1585203129),
(13,'000','16','填空题',0,'5','5','CN,,,,,,,,,','',1585203147),
(14,'000','12','简答题',1,'5','5','',';;;硬件系统',1585203363),
(16,'000','9','正式测试',2,'2,3,4,5,5,5,5,5,5,5','0,0,0,0,0,0,0,0,0,0',',,,,,,,,,',';硬件系统;;',1585912726),
(17,'000','12','简答题',0,'5','5','',';;;',1585916023),
(18,'000','12','简答题',0,'5','5','',';;;',1587266422),
(19,'000','9','正式测试',8,'2,4,5,5,3,5,5,5,5,5','0,0,1,1,0,0,0,0,0,2',',,,,,,,,,',';;;',1587266491);

UNLOCK TABLES;

/*Table structure for table `short` */

DROP TABLE IF EXISTS `short`;

CREATE TABLE `short` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '题号',
  `role` tinyint(2) unsigned NOT NULL DEFAULT '4' COMMENT '1选择题 2判断题 3填空题 4简单题',
  `content` text NOT NULL COMMENT '题目详情',
  `ans` text NOT NULL COMMENT '答案关键字',
  `value` int(2) NOT NULL DEFAULT '10' COMMENT '分值',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `short` */

LOCK TABLES `short` WRITE;

insert  into `short`(`id`,`role`,`content`,`ans`,`value`) values 
(1,4,'简述计算机系统的组成？','硬件系统,软件系统,运算器,控制器,存储器,输出设备,系统软件,应用软件',10),
(2,4,'什么是操作系统？简述操作系统分类。','管理系统资源,控制程序执行,改善人机界面,提供各种服务,合理组织计算机工作流程和用户有效使用计算机提供良好运行环境,进程与处理机管理,作业管理,设备管理,文件管理',10),
(3,4,'什么是计算机网络，常用的网络有几种类型？','地理位置不同且具有独立功能的计算机,通过通信设备和线路相互连接起来的通信系统,用以实线信息的传输和共享,总线型,星型,环形,树形,网状,局域网,企业网,广域网,铜缆网,光纤网,无线网络',10),
(4,4,'简述我的电脑、 网上邻居、 回收站的主要功能？','设定虚拟内存,检查应用软件或者驱动程序,Windows操作系统资源管理器,显示指向共享计算机,打印机和网络上其他资源的快捷方式,存放用户临时删除的文档资料,占用原磁盘存储空间',10),
(6,4,'个人计算机接入因特网的方式有哪几种？各有哪些特点？','拨号上网,带宽最大只能到56k,局域网络接入,定时令牌协议,多种拓扑结构,铺设较远距离光纤,成本较高,xDSL接入,接入方便,下载宽带高,费用低廉,有线电视网接入,非常高效,不需重复布线,电力线网络接入,主要特点是投资少,连接方便,传输速率高,有安全性,使用范围广,无线网络接入,方便、灵活、有效距离内,可以使用,信号受周围环境影响会导致不稳定现象,传输速度较慢,NAT技术,节省合法的注册地址,在地址重叠时提供解决方案,提高连接到因特网的灵活性,在网络发生变化时避免重新编址,地址转换将增加交换延迟,无法进行端到端IP跟踪,导致有些应用程序无法正常运行',10);

UNLOCK TABLES;

/*Table structure for table `student` */

DROP TABLE IF EXISTS `student`;

CREATE TABLE `student` (
  `id` varchar(11) NOT NULL COMMENT '学号',
  `name` varchar(11) NOT NULL COMMENT '姓名',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `grade_id` varchar(50) DEFAULT NULL COMMENT '班级号',
  `sex` int(2) DEFAULT NULL COMMENT '性别 1男 2女',
  `role` tinyint(2) NOT NULL DEFAULT '4' COMMENT '角色 4学生',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `mobile` varchar(20) DEFAULT NULL COMMENT '手机号',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(1) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `student` */

LOCK TABLES `student` WRITE;

insert  into `student`(`id`,`name`,`password`,`grade_id`,`sex`,`role`,`email`,`mobile`,`create_time`,`update_time`) values 
('201601','王大锤','e10adc3949ba59abbe56e057f20f883e','1621805',1,4,'123@123.com','12312312312',1584022596,1584022596),
('201602','张小炮','e10adc3949ba59abbe56e057f20f883e','1621806',1,4,'321@123.com','11223322112',1584022596,1584254587),
('201603','王小锤','e10adc3949ba59abbe56e057f20f883e','1621807',1,4,'123@123.com','12312312312',1584022596,1584198265),
('000','学生测试员','e10adc3949ba59abbe56e057f20f883e','',1,4,'1123@11.com','1123',1584022596,1584518408),
('201607','王大锤2','e10adc3949ba59abbe56e057f20f883e','1621806',1,4,'123@123.com','12312312312',1584022596,1584022596),
('2016067','王大锤3','e10adc3949ba59abbe56e057f20f883e','1621806',1,4,'123@123.com','12312312312',1584022596,1584022596),
('201605','王大锤4','e10adc3949ba59abbe56e057f20f883e','1621806',1,4,'123@123.com','12312312312',1584022596,1584022596),
('2016055','王大锤5','e10adc3949ba59abbe56e057f20f883e','1621806',1,4,'123@123.com','12312312312',1584022596,1584022596),
('2016058','王大锤6','e10adc3949ba59abbe56e057f20f883e','1621806',1,4,'123@123.com','12312312312',1584022596,1584022596);

UNLOCK TABLES;

/*Table structure for table `teacher` */

DROP TABLE IF EXISTS `teacher`;

CREATE TABLE `teacher` (
  `id` varchar(11) NOT NULL COMMENT '工号',
  `name` varchar(11) NOT NULL COMMENT '姓名',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `grade_id` varchar(50) DEFAULT NULL COMMENT '班级号',
  `sex` int(2) DEFAULT NULL COMMENT '性别 1男 2女',
  `role` tinyint(2) NOT NULL DEFAULT '3' COMMENT '角色 3教师',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `mobile` varchar(20) DEFAULT NULL COMMENT '手机号',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(1) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `teacher` */

LOCK TABLES `teacher` WRITE;

insert  into `teacher`(`id`,`name`,`password`,`grade_id`,`sex`,`role`,`email`,`mobile`,`create_time`,`update_time`) values 
('000','教师测试员','e10adc3949ba59abbe56e057f20f883e','1621806',1,3,'32111@321.com','3232',1584022596,1584517636),
('222','山大王','e10adc3949ba59abbe56e057f20f883e','1621808',1,3,'223@222.com','23232323232',1584022596,1584261960),
('444','店小二','e10adc3949ba59abbe56e057f20f883e','1621805',1,3,'444@444.com','44444444444',NULL,NULL),
('555','店小四','e10adc3949ba59abbe56e057f20f883e','1621807',2,3,'555@555.com','55555555555',NULL,1584198061),
('111','二号测试员','e10adc3949ba59abbe56e057f20f883e',NULL,1,3,NULL,NULL,NULL,NULL);

UNLOCK TABLES;

/*Table structure for table `tof` */

DROP TABLE IF EXISTS `tof`;

CREATE TABLE `tof` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '题号',
  `role` tinyint(2) unsigned NOT NULL DEFAULT '2' COMMENT '1选择题 2判断题 3填空题 4简单题',
  `content` text NOT NULL COMMENT '题目详情',
  `ans` tinyint(2) NOT NULL COMMENT '答案 1对 2错',
  `value` int(2) NOT NULL DEFAULT '2' COMMENT '分值',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

/*Data for the table `tof` */

LOCK TABLES `tof` WRITE;

insert  into `tof`(`id`,`role`,`content`,`ans`,`value`) values 
(1,2,'冯·诺依曼原理是计算机的唯一工作原理。',2,2),
(2,2,'计算机能直接识别汇编语言程序。',2,2),
(3,2,'计算机能直接执行高级语言源程序。',2,2),
(4,2,'计算机掉电后，  ROM 中的信息会丢失。',2,2),
(5,2,'计算机掉电后，外存中的信息会丢失。',1,2),
(6,2,'应用软件的作用是扩大计算机的存储容量。',2,2),
(7,2,'操作系统的功能之一是提高计算机的运行速度。',2,2),
(8,2,'一个完整的计算机系统通常是由硬件系统和软件系统两大部分组成的。',1,2),
(9,2,'第三代计算机的逻辑部件采用的是小规模集成电路。',1,2),
(10,2,'字节是计算机中常用的数据单位之一，它的英文名字是byte。',1,2),
(11,2,'计算机发展的各个阶段是以采用的物理器件作为标志的。',1,2),
(12,2,'CPU是由控制器和运算器组成的。',1,2),
(13,2,'1GB 等于 1000MB ，又等于  1000000KB。',2,2),
(14,2,'计算机软件按其用途及实现的功能不同可分为系统软件和应用软件两大类。',1,2),
(15,2,'RAM 中的信息在计算机断电后会全部丢失。',1,2),
(16,2,'中央处理器和主存储器构成计算机的主体，称为主机。',1,2),
(17,2,'主机以外的大部分硬件设备称为外围设备或外部设备，简称外设。',1,2),
(18,2,'任何存储器都有记忆能力，其中是信息不会丢失。',2,2),
(19,2,'操作系统的功能之一是提高计算机的运行速度。',2,2),
(20,2,'通常硬盘安装在主机箱内，因此它属于主存储器。',2,2);

UNLOCK TABLES;

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `name` varchar(50) NOT NULL COMMENT '用户名',
  `password` varchar(32) NOT NULL COMMENT '用户密码',
  `role` tinyint(2) unsigned NOT NULL COMMENT '角色 0/1超级管理员 2管理员',
  `sex` int(2) DEFAULT NULL COMMENT '性别 1男 2女',
  `mobile` varchar(20) DEFAULT NULL COMMENT '手机号',
  `email` varchar(255) DEFAULT NULL COMMENT '用户邮箱',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

/*Data for the table `user` */

LOCK TABLES `user` WRITE;

insert  into `user`(`id`,`name`,`password`,`role`,`sex`,`mobile`,`email`,`create_time`,`update_time`) values 
(1,'admin','e10adc3949ba59abbe56e057f20f883e',0,NULL,'00000',NULL,1583826729,1583826729),
(2,'罗文磊','e10adc3949ba59abbe56e057f20f883e',1,1,'19919919919','lwl123@qq.com',1583826729,1584255426),
(11,'管理4','63ee451939ed580ef3c4b6f0109d1fd0',2,2,'11111111111','1234@qq.com',1584033425,1584198419),
(5,'管理','e10adc3949ba59abbe56e057f20f883e',2,1,'11111111112','aaa@qq.com',1583826729,1583826729),
(10,'管理2','4297f44b13955235245b2497399d7a93',2,1,'12211111111','123@123.COM',1584032995,1584032995),
(7,'管理33','14e1b600b1fd579f47433b88e8d85291',2,1,'12332112332','aaa@qq.com',1583826729,1584022596),
(14,'管理8','e10adc3949ba59abbe56e057f20f883e',2,1,'12111111111','a@a.com',NULL,NULL),
(20,'123','e10adc3949ba59abbe56e057f20f883e',2,1,'12312322222','333@3',1584251320,1584251320);

UNLOCK TABLES;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
